# [Kauno alėja](kauno-aleja) review

- HTML
    - [x] `article` wrapped by `a` element
        * There would be no clean way of selecting `a` via CSS without classes
          or ids; if elements were the other way around, one could select `a`
          with `article > a` (`a`, immediate child of `article`)

    - [x] `div.side` in `aside`
        * Cannot see clear reason why there should be a <div> in use, since the
          same class could be applied to <aside> itself.

        > Jurgis >
        > .side just looked better, when editing at first, because of the wee
        > dot (both of the other columns are divs, so why not this one) at
        > the front.

    - [x] Semantic section markup
        * Seems to me that since `section` elements are used to represent each
          category separately, the same logic could be applied separating
          modules in the main `aside` element, i.e. 'Newest articles', 'Spam'
          or 'Popular articles' could be treated as `section`s as well.

        > Jurgis >
        > left the ids there just for future refference, I'm SURE will
        > need them.

    - [/] `body>header` and asides
        * Thinking about the website as a newspaper it would seem that the
          whole masthead area would go into `body>header` with all its
          supporting elements such as `div.weather` and `input[type="search"]`

        > Jurgis >
        > both `input`and `div.weather` put into `body>header`, but still
        > require some fine tuning when the resolution goes below 768px wide.
        > Manageable, but not quite pretty enough.

    - [x] Rogue `input[type="search"]` just before `body>header`
        * `input`s should _probably_ be wrapped with a `form` element since it
          is so easily overlooked until submitted for production and the devs
          suddenly fuck up the page layout beyond all recognition.

        > Jurgis >
        > Now THIS, f****d my positioning, it has.

- CSS
    - [x] `div.weather` positioning
        * Unwritten rule of thumb is not to use `absolute` positioning if the
          parent element is expected to adapt itself to its children.

        > Jurgis >
        > Roger that, no absolute positioning. Rainbows and unicorns now.

- [X] Browser test
    * Release candidates should be properly browser tested at least on Windows
      and OS X (or, dare I say, _other_ unix-like OS) using all major browsers,
      namely Chrome, FireFox, Opera, Safari (when applicable), Internet
      Explorer (7 or 8 through 9 or 10). Just for laughs, Maxthon could be
      considered too.

    > Jurgis >
    > Works on IE 6 SP 3 - FUCKYEAH!

- [ ] Keyboard navigation
    * An issue not considered a real one here at 4444, but in my personal
      opinion (_and_ by EU law) websites should be built with handicapped
      users in mind -- test your might, grasshopper. *HYAH!*

---
[kauno-aleja]: http://design.karolis.jaleja.tit.lt/